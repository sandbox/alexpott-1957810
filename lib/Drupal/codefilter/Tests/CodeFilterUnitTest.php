<?php

/**
 * @file
 * Contains \Drupal\codefilter\Tests\CodeFilterUnitTest.
 */

namespace Drupal\codefilter\Tests;

use Drupal\simpletest\DrupalUnitTestBase;

class CodeFilterUnitTest extends DrupalUnitTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('codefilter');

  // The filter object as returned from codefilter_filter_info().
  private $filter;

  public static function getInfo() {
    return array(
      'name' => 'Codefilter module text filters',
      'description' => 'Tests raw filtering functions.',
      'group' => 'Code Filter',
    );
  }

  protected function setUp() {
    parent::setUp();
    $this->filter = codefilter_filter_info();
    $this->path = drupal_get_path('module', 'codefilter') . '/tests';
  }

  /**
   * Filters text through codefilters prepare and process callbacks.
   *
   * @param string $text
   *   The text to filter.
   *
   * @return string
   *   The processed text.
   */
  protected function filterText($text, $settings = array()) {
    $filter =& $this->filter['codefilter'];
    // Set up a dummy format using defaults.
    $format = new \stdClass();
    $format->settings = array_merge($filter['default settings'], $settings);
    $text = call_user_func($filter['prepare callback'], $text, $format);
    $text = call_user_func($filter['process callback'], $text, $format);
    return $text;
  }

  /**
   * Checks that <?php tags are escaped and highlighted correctly.
   */
  function testPhpFilter() {
    $input = file_get_contents($this->path . '/codefilter.php-input.txt');
    $expected = file_get_contents($this->path . '/codefilter.php-output.txt');
    $result = $this->filterText($input);
    $this->assertIdentical($expected, $result, 'PHP tags are filtered.');
  }

  /**
   * Checks that <code> tags are escaped and highlighted correctly.
   */
  function testCodeFilter() {
    $input = file_get_contents($this->path . '/codefilter.code-input.txt');
    $expected = file_get_contents($this->path . '/codefilter.code-output.txt');
    $result = $this->filterText($input);
    $this->assertIdentical($expected, $result, 'Code tags are filtered.');
  }

  /**
   * Checks that CSS classes are added which JS uses for hover events.
   */
  function testContainerExpand() {
    $input = file_get_contents($this->path . '/codefilter.php-input.txt');
    $settings = array(
      'nowrap_expand' => TRUE,
    );
    $result = $this->filterText($input, $settings);

    $this->assertTrue(
      strpos($result, '<div class="codeblock nowrap-expand">') !== FALSE,
      'Expand class is added to codefilter blocks that are too long when that option is specified.'
    );
  }
}
